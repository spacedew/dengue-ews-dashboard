# Script for preparing objects used in the "Early Warning System" tab

# To run the regional and local models based on the user's inputs

# This script is not run at each execution of the app. The .rda file produced by this script is loaded at each execution
# of the application (via global.R)




# Regional model: code from /home/lucas/Documents/SPACEDEW/scripts/estimate_infected_passengers.py
library(lubridate)

# Functions ---------------------------------------------------------------
get_prob <- function(gamma_cm, dm){
  return(1-exp(-gamma_cm/dm))
}
proba_infected <- function(tc, n, beta){
  return(ifelse(tc>=(n-1), 
                ((1-beta)**(tc-n+1)) - (1-beta)**tc,
                1-(1-beta)**tc))
}

# Load data ---------------------------------------------------------------


data_oag_typical_year = "data/data_for_regional_model/processed_oag_country_data_typical_year.csv" %>%
  read.csv2()
data_oag_typical_year_incoming_yearly = "data/data_for_regional_model/processed_oag_country_data_incoming_yearly_typical_year.csv" %>%
  read.csv2()
data_wto = "data/data_for_regional_model/processed_Arrivals-1995-2019.csv" %>%
  read.csv()
data_median_age = "data/data_for_regional_model/processed_WF_CIA_median_age.csv" %>%
  read.csv()
data_iamat = "data/data_for_regional_model/transm_info_2021_07_21.csv" %>%
  read.csv2()
data_oag = "data/data_for_regional_model/processed_oag_country_data.csv" %>%
  read.csv2()
data_oag_incoming_yearly = "data/data_for_regional_model/processed_oag_country_data_incoming_yearly.csv" %>%
  read.csv2()


# To determine the average proportion of residents/visitors arriving in each country
prop_vis_df <- foreach(iso_=unique(data_oag$destination_iso), .combine='rbind') %do% {
  foreach(yr = min(data_oag_incoming_yearly$year):max(data_oag_incoming_yearly$year), .combine='rbind') %do% {
    nb_p_yr <- subset(data_oag_incoming_yearly) %>%
      subset(year==yr & destination_iso == iso_) %>%
      pull(total_est_pax)
    nb_vis_yr <- data_wto %>%
      subset(iso==iso_ & year==yr) %>%
      pull(tourists)
    prop_vis <- nb_vis_yr / nb_p_yr
    prop_vis <- min(c(prop_vis, 1))
    data.frame(iso=iso_, year = yr, prop_vis=prop_vis)
  }
} %>%
  group_by(iso) %>%
  summarize(prop_vis = mean(prop_vis, na.rm=T))


# Model -------------------------------------------------------------------
compute_regional_model <- function(df, 
                                   m, 
                                   iso_destination,
                                   X=100# Nombre de tirages pour estimer la moyenne et la std
){
  require(lubridate)
  dm = sprintf("2000-%.2d-01", m) %>%# nombre de jours dans le mois
    as.Date() %>%
    days_in_month() %>%
    as.numeric()
  infected_p <- foreach(i = 1:nrow(df), .combine='rbind') %do% {
    iso_origin <- df$iso[i]
    incidence <- df$incidence[i]
    mu_vis <- data_median_age$value[data_median_age$iso==iso_origin]
    nb_p  <- data_oag_typical_year %>%
      subset(origin_iso==iso_origin & destination_iso==iso_destination & month==m) %>% 
      pull(total_est_pax)
    
    gamma_cm = incidence / 100000
    beta = get_prob(gamma_cm, dm)# probabilité journalière d'etre infecté dans le pays d'origine
    
    if (beta != 0){
      prop_vis <- prop_vis_df$prop_vis[prop_vis_df$iso==iso_destination]
      if (!is.na(prop_vis)){
        nb_vis <- int(round(nb_p * prop_vis))
        nb_res <- int(round(nb_p * (1-prop_vis)))
        tcs_vis <- rnorm(nb_vis*X, mean=mu_vis, sd=0.1*mu_vis)
        tcs_res <- rnorm(nb_res*X, mean=15, sd=2)
        ns_vis <- rgamma(nb_vis*X, shape=25, scale=0.2)#  IIP + infectious period in days
        ns_res <- rgamma(nb_res*X, shape=25, scale=0.2)#  IIP + infectious period in days
        
        probas_infected_vis = proba_infected(tcs_vis, ns_vis, beta) %>%
          matrix(nrow=X)
        probas_infected_res = proba_infected(tcs_res, ns_res, beta) %>%
          matrix(nrow=X)
        
        nb_infected_vis <- apply(probas_infected_vis, 1, sum)
        nb_infected_res <- apply(probas_infected_res, 1, sum)
        nb_infected <- nb_infected_vis + nb_infected_res
        
        nb_infected_vis_mean <- mean(nb_infected_vis)
        nb_infected_vis_std <- sd(nb_infected_vis)
        nb_infected_vis_q025 <- quantile(nb_infected_vis, 0.025)
        nb_infected_vis_q975 <- quantile(nb_infected_vis, 0.975)
        nb_infected_res_mean <- mean(nb_infected_res)
        nb_infected_res_std <- sd(nb_infected_res)
        nb_infected_res_q025 <- quantile(nb_infected_res, 0.025)
        nb_infected_res_q975 <- quantile(nb_infected_res, 0.975)
        nb_infected_mean <- mean(nb_infected)
        nb_infected_std <- sd(nb_infected)
        nb_infected_q025 <- quantile(nb_infected, 0.025)
        nb_infected_q975 <- quantile(nb_infected, 0.975)
      }else{
        nb_infected_vis_mean = nb_infected_vis_std = nb_infected_vis_q025 = nb_infected_vis_q975 = nb_infected_res_mean = nb_infected_res_std = nb_infected_res_q025 = nb_infected_res_q975 = nb_infected_mean = nb_infected_std = nb_infected_q025 = nb_infected_q975 = 0
      }
    }else{
      nb_infected_vis_mean = nb_infected_vis_std = nb_infected_vis_q025 = nb_infected_vis_q975 = nb_infected_res_mean = nb_infected_res_std = nb_infected_res_q025 = nb_infected_res_q975 = nb_infected_mean = nb_infected_std = nb_infected_q025 = nb_infected_q975 = 0
    }
    c(iso_origin, m, nb_infected_vis_mean, nb_infected_vis_std, nb_infected_vis_q025, nb_infected_vis_q975, 
      nb_infected_res_mean, nb_infected_res_std, nb_infected_res_q025, nb_infected_res_q975,  
      nb_infected_mean, nb_infected_std, nb_infected_q025, nb_infected_q975) %>%
      t() %>% 
      data.frame() %>% 
      purrr::set_names(c("iso_origin","month",
                         "nb_infected_vis_mean", "nb_infected_vis_std", "nb_infected_vis_q025", "nb_infected_vis_q975", 
                         "nb_infected_res_mean", "nb_infected_res_std", "nb_infected_res_q025", "nb_infected_res_q975",  
                         "nb_infected_mean", "nb_infected_std", "nb_infected_q025", "nb_infected_q975"))
  } %>%
    mutate(across(matches("nb_infected"), ~ as.numeric(.x))) %>%
    group_by(iso_origin) %>%
    summarize(nb_infected_mean = sum(nb_infected_mean),
              .groups="drop")
  
  summup <- sprintf(
    "This month (%s):</br>
    <p>
      <center>
        <b>%s</b> incoming infected passengers from <b>%s</b> countries 
      </center>
    </p>",
    strftime(Sys.time(), format="%b %Y"),
    round(sum(infected_p$nb_infected_mean), digits=2),
    nrow(infected_p)
  )
  detailed <- sprintf(
    "
    <p>
      <center>
        %s 
      </center>
    </p>",
    sapply(1:nrow(df), function(i){
      sprintf("<b>%s</b> incoming infected passengers from <b>%s</b>",
              round(infected_p$nb_infected_mean[i], digits=2),
              country_info$country[match(infected_p$iso_origin[i], country_info$iso)])
    }) %>%
      paste(collapse='</br>')
  )
  
  return(paste(c(summup, detailed), collapse="</br>"))
}


# Add panels for input data -----------------------------------------------
add_regional_fields <- function(i, choices_countries, input, iso=NULL) {
  choices_countries <- country_info$country[match(choices_origin_isos_for_ews, country_info$iso)]
  if (is.null(iso)){iso="AUS"}
  fluidRow(
    id = "ui_ews_regional",
    # id = sprintf("ui_ews_regional__%s",i),
    box(width=6,
        selectInput(inputId = sprintf("regional_model_country%s", i), 
                    label = "Country",
                    choices = choices_countries,
                    selected = country_info$country[match(iso, country_info$iso)])),
    box(width=6,
        numericInput(inputId = sprintf("regional_model_incidence%s", i), 
                     label = "Incidence (# per 100,000)",
                     value=round(median_incidence_who$median_incidence[median_incidence_who$iso==iso],
                                 digits=2)
                       ))
  )
}


# For each destination country, which countries are the main source of infected passengers ?
load('data/Data_regional_model.rda')
top_export_countries <- regional_model_data[["WHO"]] %>%
  group_by(from, to) %>%
  summarize(nb_infected_mean = sum(nb_infected_mean), .groups="drop") %>%
  arrange(to, desc(nb_infected_mean)) %>%
  group_by(to) %>%
  mutate(rank = rank(desc(nb_infected_mean))) %>%
  subset(rank<=10) %>%
  dplyr::select(-nb_infected_mean, -rank) %>%
  nest(data=from)


# Functions used for adding user-input fields
add_regional_fields <- function(i, iso=NULL, incidence=NULL) {
  choices_countries <- country_info$country[match(choices_origin_isos_for_ews, country_info$iso)]
  if (is.null(iso)){iso="AUS"}
  if (is.null(incidence)){
    value = median_incidence_who$median_incidence[median_incidence_who$iso==iso]
  }else{
    value = incidence
  }
  fluidRow(
    box(width=6,
        selectInput(inputId = sprintf("regional_model_country%s", i), 
                    label = "Country",
                    choices = choices_countries,
                    selected = country_info$country[match(iso, country_info$iso)])),
    box(width=6,
        numericInput(inputId = sprintf("regional_model_incidence%s", i), 
                     label = "Incidence (# per 100,000)",
                     value=round(value, digits=2)
        ))
  )
}

ui_ews_regional <- function(index_ews_regional, input, initial_settings=T, add_country=F, empty=F){
  uis = tagList()
  if (!empty){
    top_isos <- top_export_countries %>%
      subset(to==country_info$iso[match(input$choice_country_ews, country_info$country)]) %>%
      pull(data) %>%
      magrittr::extract2(1) %>%
      pull(from)
    if (initial_settings){
      for (i in 1:3){
        uis[[i]] <- add_regional_fields(i, iso=top_isos[i])
      } 
    }else{
      countries <- map_chr(paste0("regional_model_country", seq_len(index_ews_regional$i-1)), ~ input[[.x]] %||% "")
      incidences <- map_chr(paste0("regional_model_incidence", seq_len(index_ews_regional$i-1)), ~ input[[.x]] %||% "") %>%
        as.numeric()
      for (i in 1:(index_ews_regional$i-1)){
        uis[[i]] <- add_regional_fields(i, iso=country_info$iso[match(countries[i], country_info$country)], incidence=incidences[i])
      } 
    }
    if (add_country){
      countries <- map_chr(paste0("regional_model_country", seq_len(index_ews_regional$i-1)), ~ input[[.x]] %||% "")
      isos <- country_info$iso[match(countries, country_info$country)]
      next_iso <- top_isos[!(top_isos %in% isos)][1]
      if (is.na(next_iso)){next_iso = "AUS"}
      uis[[index_ews_regional$i]] <- add_regional_fields(index_ews_regional$i, iso=next_iso)
    }
  }
  uis
}


save(data_oag_typical_year, data_oag_typical_year_incoming_yearly, data_wto, data_median_age, data_iamat, data_oag, data_oag_incoming_yearly, 
     prop_vis_df, compute_regional_model, add_regional_fields, get_prob, proba_infected, top_export_countries, ui_ews_regional,
     file= 'data/Data_ews_regional_model.rda')


