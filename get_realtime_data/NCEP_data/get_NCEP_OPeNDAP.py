#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 10:12:35 2022

@author: lucas

Pour télécharger les données du modèle ré-analysé NCEP via OPeNDAP, en un point partiulier

Catalogue des modèles disponibles:
    https://psl.noaa.gov/data/gridded/tables/precipitation.html
    https://psl.noaa.gov/data/gridded/tables/temperature.html
    
Catalogue pour données NCEP Reanalysis:
    https://psl.noaa.gov/thredds/catalog/Datasets/ncep.reanalysis/catalog.html


Catalogue des datasets "surface":
    https://psl.noaa.gov/thredds/catalog/Datasets/ncep.reanalysis.derived/surface/catalog.html


Script issus de /home/lucas/Documents/SPACEDEW/scripts/data/get_NCEP_OPeNDAP.py

"""
from pydap.client import open_url
import numpy as np
import pandas as pd
from cftime._cftime import num2date, date2num
import datetime
from functools import reduce
# url_source = "https://psl.noaa.gov/thredds/dodsC/Datasets/ncep.reanalysis.derived/surface"
# urls = {'air temperature': ['air.mon.mean.nc', 'air'],
#         'relative humidity': ['rhum.mon.mean.nc', 'rhum']}




url_source = "https://psl.noaa.gov/thredds/dodsC/Datasets/ncep.reanalysis/Monthlies/"
urls = {'air temperature': ['surface/air.mon.mean.nc', 'air'],
        'relative humidity': ['surface/rhum.mon.mean.nc', 'rhum'],
        'precipitation rate': ['surface_gauss/prate.mon.mean.nc', 'prate']}
# urls = {'air temperature': ['surface/air.mon.mean.nc', 'air'],
#         'unknown1': ['surface/air.mon.ltm.nc', 'air'],
#         'unknown2': ['surface/air.day.ltm.nc', 'air']}



locations = [['suva', 178.44, -18.13],
             ['noumea', 166.46, -22.27]]
# starttime = datetime.datetime(2015,1,1)
starttime = datetime.datetime.today() - datetime.timedelta(days=30)

for location in locations:
    print("Downloading NCEP data from {}...".format(location[0]))
    lon, lat = location[1], location[2]
    df_list = []
    for key in urls:
        url = "{0}{1}".format(url_source, urls[key][0])
        dataset = open_url(url)
        for_lons = dataset['lon']
        for_lats = dataset['lat']
        for_time = dataset['time']
        for_var = dataset[urls[key][1]]
        lons = for_lons[0:143]# this will download data from the server
        lats = for_lats[0:72]
        times = for_time[0:-1]    
        
        ilon, ilat = np.argmin(abs(lons.data-lon)), np.argmin(abs(lats.data-lat))
        istarttime = np.argmin(abs(times.data-date2num(starttime, units=for_time.attributes['units'])))
        var = for_var[istarttime:-1, ilat:(ilat+1), ilon:(ilon+1)]
    
        # If the data is big endian, swap the byte order to make it little endian
        values = var.data[0][:,0,0] 
        if values.dtype.byteorder == '>':
            values = values.byteswap().newbyteorder()
        
        times = num2date(var.data[1] , units=for_time.attributes['units']) 
        array = np.array([[str(datetime.date(t.year, t.month, t.day)) for t in times], 
                                   values]).transpose()
        df = pd.DataFrame(array, columns=['time',key])
        df[key] = df[key].astype('float64')
        df_list.append(df)
    df = reduce(lambda df1,df2: pd.merge(df1,df2,on=['time']), df_list)
    historic_df = pd.read_csv("../../data/realtime_data/ncep_{0}.csv".format(location[0]))
    new_df = pd.concat([historic_df, df])
    new_df = new_df.drop_duplicates(subset=['time'], keep='last')
    new_df.to_csv("../../data/realtime_data/ncep_{0}.csv".format(location[0]), index=False)
    print("NCEP data from {} saved".format(location[0]))
