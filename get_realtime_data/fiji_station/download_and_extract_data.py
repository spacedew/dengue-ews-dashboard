#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 28 16:05:17 2022

Utilisable via l'environnement conda opencv_env

@author: lucas
"""

# =============================================================================
# Download Image file
# =============================================================================
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains 
from selenium.webdriver.common.keys import Keys
import datetime
import pandas as pd
import numpy as np
import sys
import os

from selenium.webdriver.firefox.options import Options as FirefoxOptions
options = FirefoxOptions()
options.add_argument("--headless")# pour ne pas ouvrir une fenetre du navigateur

url_source = "https://www.weatheronline.co.uk/weather"
url = "https://www.weatheronline.co.uk/weather/maps/city?WMO=91680&CONT=aupa&LAND=FJ&ART=MIN&LEVEL=150"

filename = "import_{0}".format(datetime.datetime.today().strftime("%Y_%m_%d"))

    # On ouvre la page dans le navigateur
print("Opening web browser...")
driver = webdriver.Firefox(options=options)
driver.get(url)

print("Fetching image on {0}...".format(url))
    # On clique sur le popup "Consent" pour les cookies
button = driver.find_element(By.CLASS_NAME, "fc-button.fc-cta-consent.fc-primary-button")
button.click()

    # On cherche l'image (2e élément de la classe zentrier)
class1 = driver.find_element(By.CLASS_NAME, "zentrier")
class2 = class1.find_element(By.CLASS_NAME, "zentrier")
img = class2.find_element_by_tag_name("img")
filepath = "downloads/{0}.png".format(filename)
print("Saving image to {0}...".format(os.path.abspath(filepath)))
with open(filepath, 'wb') as file:
    file.write(img.screenshot_as_png)

driver.quit()

# =============================================================================
# Extract data
# =============================================================================
from PIL import Image 
import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
import cv2
import re
import time
import datetime
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
import argparse
import matplotlib.dates as mdates
import scipy.ndimage as ndimage
from pytesseract import pytesseract # pour installer tesseract https://stackoverflow.com/questions/50951955/pytesseract-tesseractnotfound-error-tesseract-is-not-installed-or-its-not-i
path_to_tesseract = r'/usr/bin/tesseract'
pytesseract.tesseract_cmd = path_to_tesseract



area = (8, 15, 329, 496)
xlimit = 24# valeur de x à gauche de laquelle on a les valeurs des ordonnées
ylimit = 302# valeur de y au dessus de laquelle on a les valeurs des abscisses
path = filepath
output_path = "outputs/{0}".format(os.path.basename(path).replace(".png", ".csv"))


# Lecture de l'image recadrée dans ses 3 canaux rouge vert bleu
image = cv2.imread(path)
h, w, _ = image.shape
resized = image[(h-area[2]):(h-area[0]), area[1]:area[3], :]
r = resized[:,:,2]
g = resized[:,:,1]
b = resized[:,:,0]

    # on ne garde que la couleur bleue
only_blue = np.where((r<b) & (g<b), 1, 0)

    # on ne garde que les pixels qui ont des voisins bleus (pour éliminer les lignes entre les points)
def test_func(values):
    return values.sum()
footprint = np.array([[1,1,1,1,1],
                      [1,1,1,1,1],
                      [1,1,1,1,1],
                      [1,1,1,1,1],
                      [1,1,1,1,1]])
sum_neighbours = ndimage.generic_filter(only_blue, test_func, footprint=footprint)
points = np.where(sum_neighbours>=23, 1, 0)
# footprint = np.array([[0,0,0,0,0],
#                       [0,0,0,0,0],
#                       [1,1,1,1,1],
#                       [0,0,0,0,0],
#                       [0,0,0,0,0]])
# sum_neighbours = ndimage.generic_filter(only_blue, test_func, footprint=footprint)
# points = np.where(sum_neighbours>=5, 1, 0)

plt.ioff()
fig = plt.figure(constrained_layout=False, figsize=(20,12))
spec = fig.add_gridspec(ncols=3, nrows=2, left=0.05, right=0.95, bottom=0.05, top=0.95)
ax = fig.add_subplot(spec[0,0])
ax.imshow(np.flip(image, axis=2))
ax.set_title("Source image")

ax = fig.add_subplot(spec[0,1])
ax.imshow(np.flip(resized, axis=2))
ax.axhline(ylimit, color='r')
ax.axvline(xlimit, color='r')
ax.set_title("Resized image")

ax = fig.add_subplot(spec[0,2])
ax.imshow(points)
# ax.imshow(sum_neighbours)
ax.set_title("Only blue points")

#####################################
# 1 - Identification des centroides via Kmeans   (avec identification automatique du nombre optimal de clusters)  
#####################################
print("Detection of centroids")
def optimalK(data, nrefs=3, minClusters=24, maxClusters=35, plot_=False):
    resultsdf = pd.DataFrame({'clusterCount':[], 'silhouette_score':[]})
    for k in range(minClusters, maxClusters):
        km = KMeans(k)
        km.fit(data)
        ss = silhouette_score(array, km.labels_, metric='euclidean')
        resultsdf=resultsdf.append({'clusterCount':k, 'silhouette_score':ss}, ignore_index=True)
    opt_k = int(resultsdf.iloc[resultsdf.silhouette_score.argmax()].clusterCount)
    if plot_:
        fig, ax = plt.subplots()
        ax.plot(df['clusterCount'], df['silhouette_score'], linestyle='--', marker='o', color='b')
        ax.set_xlabel('K')
        ax.set_ylabel('Gap Statistic')
        fig.suptitle('Gap Statistic vs. K; Optimal k = {0}'.format(opt_k))
    return (opt_k, resultsdf)  
array = np.array(np.where(points==1)).transpose()
opt_k, df = optimalK(array, nrefs=3, minClusters=24, maxClusters=35)

n_clusters = opt_k
kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(array)
pixel_groups = pd.DataFrame({'x': array[:,1], 'y': array[:,0], 'group':kmeans.labels_})
centroids = pixel_groups.groupby('group')[['x','y']].mean()

# Pour plotter les pixels et leurs centroides
ax = fig.add_subplot(spec[1,0])
ax.imshow(points)
centroids.plot.scatter(x='x', y='y', marker='o', color='r', ax=ax, s=10)
ax.set_title("Detection of centroids")

#####################################
# 2 - Identification des références sur les axes X et Y  
#####################################
print("Detection of axis references and labels")

grid = b.copy() # on extrait seulement la grille
# pour éliminer tout ce qui est au centre 
columns = np.array([j for j in range(grid.shape[1]) if j >= xlimit], dtype=np.intp)
rows = np.array([i for i in range(grid.shape[0]) if i <= ylimit], dtype=np.intp)
grid[rows[:, np.newaxis], columns] = 255

ax = fig.add_subplot(spec[1,1])
ax.imshow(grid)
ax.set_title("Grid")


# Identification du texte au sein de l'image avec l'outil tesseract
textboxes = pytesseract.image_to_boxes(grid)
textboxes_df = pd.DataFrame([t.split(' ') for t in textboxes.split('\n') if t!=''], 
                            columns = ['text', 'x1', 'y1', 'x2', 'y2', 'z']).drop(columns='z')
textboxes_df = textboxes_df.astype({'text':'str', 'x1':'int32', 'y1':'int32', 'x2':'int32', 'y2':'int32'})
textboxes_df.y1 = grid.shape[0] - textboxes_df.y1
textboxes_df.y2 = grid.shape[0] - textboxes_df.y2
textboxes_df['x'] = (textboxes_df['x1'] + textboxes_df['x2']) / 2
textboxes_df['y'] = (textboxes_df['y1'] + textboxes_df['y2']) / 2
yaxis = textboxes_df.loc[textboxes_df.x2 <= xlimit].drop(columns=['x1', 'y1', 'x2', 'y2']).sort_values(by='x').sort_values(by='y').reset_index(drop=True)
xaxis = textboxes_df.loc[textboxes_df.y2 >= ylimit].drop(columns=['x1', 'y1', 'x2', 'y2']).sort_values(by='x').reset_index(drop=True)

# Extraction de la référence Y
    # on regroupe les labels ayant un y similaire
yaxis['group'] = 0
g = 1
i = 0
while i < yaxis.shape[0]:
    y = yaxis.iloc[i].y
    indices_group = np.where(abs(yaxis.y - y) <= 0.5)
    if len(indices_group[0]) > 1:
        yaxis.at[indices_group[0], 'group'] = g
        i = max(indices_group[0]) + 1
    else:
        yaxis.at[i, 'group'] = g
        i += 1
    g += 1
yaxis2 = pd.DataFrame({'text':[], 'x':[], 'y':[], 'group':[]})
for g in np.unique(yaxis.group):
    sub_yaxis = yaxis.loc[yaxis.group==g]
    text = ''.join(sub_yaxis.text.tolist())
    yaxis2 = yaxis2.append({'text': text, 'x':np.mean(sub_yaxis.x), 'y':np.mean(sub_yaxis.y), 'group':g}, 
                            ignore_index=True)
    # on ne conserve que les nombres entre 10 et 40°C
ref_yaxis = yaxis2.loc[[re.match(r"[0-9]+$", t) is not None for t in yaxis2.text.tolist()]]
ref_yaxis = ref_yaxis.astype({'text': float})
ref_yaxis = ref_yaxis.loc[(ref_yaxis.text > 10) & (ref_yaxis.text < 40)].reset_index(drop=True)


# Extraction de la référence X (pas forcément nécessaire? si on considère que les 2 bords de la fenetre sont des dates fixes par rapport à la date actuelle)
months = [d.strftime("%b") for d in [datetime.date(2000, m+1, 1) for m in range(12)]]
    # on regroupe les labels ayant un y similaire
xaxis['group'] = 0
g = 1
i = 0
while i < xaxis.shape[0]:
    x = xaxis.iloc[i].x
    indices_group = np.where(abs(xaxis.x - x) <= 40)
    if len(indices_group[0]) > 1:
        xaxis.at[indices_group[0], 'group'] = g
        i = max(indices_group[0]) + 1
    else:
        xaxis.at[i, 'group'] = g
        i += 1
    g += 1
xaxis['isnb'] = [re.match(r"[0-9]+$", t) is not None for t in xaxis.text.tolist()]
xaxis['istext'] = [re.match(r"[a-zA-Z]+$", t) is not None for t in xaxis.text.tolist()]
xaxis = xaxis.loc[(xaxis.isnb) | (xaxis.istext)].reset_index(drop=True)
xaxis2 = pd.DataFrame({'day':[], 'month':[], 'x':[], 'y':[], 'group':[]})
for g in np.unique(xaxis.group):
    sub_xaxis = xaxis.loc[xaxis.group==g]
    day = int(''.join(sub_xaxis.loc[sub_xaxis.isnb].text.tolist()))
    month = ''.join(sub_xaxis.loc[sub_xaxis.istext].text.tolist())
    xaxis2 = xaxis2.append({'day': day, 'month':month, 'x':np.mean(sub_xaxis.x), 'y':np.mean(sub_xaxis.y), 'group':g}, 
                            ignore_index=True)
xaxis2.day = xaxis2.day.astype(int)
xaxis2 = xaxis2.loc[xaxis2.month.isin(months)].reset_index(drop=True)
xaxis2['m'] = [months.index(month)+1 for month in xaxis2.month.tolist()]

def correctDate(year, month, day):
    correctDate = None
    try:
        newDate = datetime.datetime(year,month,day)
        correctDate = True
    except ValueError:
        correctDate = False
    return(correctDate)
ref_xaxis = xaxis2.loc[[correctDate(datetime.datetime.today().year, d.m, d.day) for id_, d in xaxis2.iterrows()]].reset_index(drop=True)
ref_xaxis['date'] = pd.to_datetime([datetime.date(datetime.datetime.today().year, d.m, d.day) for id_, d in ref_xaxis.iterrows()])
ref_xaxis['datenum'] = (ref_xaxis.date - np.datetime64('1970-01-01')).dt.days



#####################################
# 2 - Calcul des coordonnées des centroides avec les références du cadre 
#####################################
print('Calculation of centroids values')
def get_values(x, y, ref_xaxis, ref_yaxis):
    refx = ref_xaxis.iloc[[0,-1]].reset_index(drop=True)
    refy = ref_yaxis.iloc[[0,-1]].reset_index(drop=True)
    xval = refx.datenum[0] + (refx.datenum[1] - refx.datenum[0])  * (x-refx.x[0]) / (refx.x[1] - refx.x[0])
    yval = refy.text[0] + ((refy.text[1] - refy.text[0]) / (refy.y[1] - refy.y[0])) * (y-refy.y[0])
    return(xval, yval)
centroids[['datenum', 'temperature']] = [get_values(elt.x, elt.y, ref_xaxis, ref_yaxis) for i, elt in centroids.iterrows()]
centroids.temperature = [round(t, 1) for t in centroids.temperature.values.tolist()]
centroids['date'] = pd.to_datetime([np.datetime64('1970-01-01') + np.timedelta64(int(round(d)), "D") for d in centroids.datenum.tolist()])
centroids = centroids.sort_values(by='datenum')

ax = fig.add_subplot(spec[1,2])
centroids.plot(x="date", y="temperature", ax=ax)
centroids.plot.scatter(x="date", y="temperature", ax=ax)
ax.set_title("Output data")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%b-%d'))
# Rotates and right-aligns the x labels so they don't crowd each other.
for label in ax.get_xticklabels(which='major'):
    label.set(rotation=30, horizontalalignment='right')
check_process_filepath = output_path.replace('outputs', 'outputs/check_process').replace('.csv','.png')
plt.savefig(check_process_filepath)

# Send check_process to email adress
email_to = "lucas.bonnin@ird.fr"
subject = "SPACEDEW EWS - download_and_extract - {0}".format(datetime.datetime.today().strftime("%Y-%m-%d"))
attachment = check_process_filepath
commande = "echo 'download_and_extract processed on {3}' | mutt -s '{0}' -a {1} -- {2}".format(subject, 
                                                                                            attachment, 
                                                                                            email_to, 
                                                                                        datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S"))
print("Sending email to {0} to check the validity of the process.".format(email_to))
os.system(commande)

#####################################
# 3 - Export data
#####################################

    # Individual file
print("Exporting CSV file")
centroids[['date', 'temperature']].to_csv(output_path, index=False)

    # tous les fichiers combinés (ayant pour but d'etre moyennés par date )
new = centroids[['date', 'temperature']].copy()
new['date_import'] = datetime.datetime.today().strftime("%Y-%m-%d")
file_complet = 'outputs/import_all.csv'
file_complet_archives = 'outputs/archives/import_all_{0}.csv'.format(datetime.datetime.today().strftime("%Y%m%d"))
all_centroids = pd.read_csv(file_complet)
all_centroids = all_centroids.append(new).reset_index(drop=True)
all_centroids.to_csv(file_complet, index=False)
all_centroids.to_csv(file_complet_archives, index=False)
all_centroids.to_csv('../../data/realtime_data/weatheronline_suva.csv', index=False)
