## Installation (Linux machine)
- Copy the dengue-ews-dashboard folder to any location
- Install the required *R* libraries (run *install_missing_R_libraries.R*)
- Modify the *restart_app* file to change the host IP and port
- Optional\*: Create *Anaconda* environment using:
```
conda env create -f instructions/install/spacedew_dashboard_env.yml
```
      
## Execution
3 files to be executed everyday for updating realtime data (Optional*):
- *get_realtime_data/NCEP_data/get_NCEP_OpeNDAP*: for the download of NCEP data in point locations (Nouméa and Suva)
- *get_realtime_data/fiji_station/download_and_extract_data*: to download temperature data in Fiji from weatheronline. This script downloads the data in PNG format (image), then extracts data from the image and sends an email to the app manager (modify destination email address in the *get_realtime_data/fiji_station/download_and_extract_data.py* python file). This may require the configuration of an origin email account.
- *restart_app*: to re-launch the app with new updated data
The execution of these files needs to be programmed each day (at 5am here) using *crontab*:
```
crontab -l
```
then insert the following lines:
```
0 5 * * * /path/to/folder/dengue-ews-dashboard/get_realtime_data/NCEP_data/get_NCEP_OPeNDAP 2>&1 >/tmp/trace_spacedew_data1.txt
0 5 * * * export DISPLAY=:0 && export PATH=$PATH:/usr/local/bin && /path/to/folder/dengue-ews-dashboard/get_realtime_data/fiji_station/download_and_extract_data 2>&1 >/tmp/trace_spacedew_data2.txt
5 5 * * * /path/to/folder/dengue-ews-dashboard/restart_app 2>&1 >/tmp/trace_restart_app.txt
```


## Application layout / file organization
- *ui.R*: user interface file
- *server.R*: server function
- *global.R*: Sets up data objects, functions, libraries, etc.
- *for_raster_data.R*: prepare the raster data at each run of the app
- *for_realtime_data.R*: prepare the realtime data at each run of the app
- *restart_app*: to restart the app, and send it to a specific IP and port (these parameters can be modified within the file)
- *www/*: contains the logos and CSS files
- *data/*: contains the data files in .rda format
  - *Rmd/*: Contains the Rmd files used for the “About” tab
- *prep_data/*: Contains various R files used to generate .rda files (only needs to be run if these files needs to be modified)
- *instructions/*: contains instructions about the app and its install



Optional\*: for updating realtime data. If not wanted:
- The __Installation__ does not need to set-up an Anaconda environment 
- The __Execution__ only needs to run restart_app (once, not everyday)
