#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 16:17:57 2022

@author: lucas
"""

import xarray as xr
from os.path import exists
import numpy as np

input_dir = "/home/lucas/Documents/SPACEDEW/dashboard/framagit/dengue-ews-dashboard/data/raster_data"

# Température
input_filepaths = ["{0}/ERA5_Land_monthly_fiji_west_19710101_20210501_t2m.nc".format(input_dir),
                   "{0}/ERA5_Land_monthly_new_caledonia_19710101_20211231_t2m.nc".format(input_dir)]

# Précipitations
input_filepaths = ["{0}/ERA5_Land_monthly_fiji_west_19710101_20210501_tp.nc".format(input_dir),
                  "{0}/ERA5_Land_monthly_new_caledonia_19710101_20211231_tp.nc".format(input_dir)]
for input_filepath in input_filepaths:
    output_filepath = input_filepath.replace(".nc", "_formatted.nc")
    if not exists(output_filepath):
        ds = xr.open_dataset(input_filepath, engine='netcdf4')
        ar = ds['tp'][:,:,:]
        ar = np.where(np.sum(ar, axis = 0) == 0, ar + np.nan, ar)
        ds['tp'][:,:,:] = ar
        ds.to_netcdf(output_filepath)


# moustiques
for species in ['aegypti','albopictus']:
    input_filepaths = ["{0}/output_{1}_ERA5_Land_monthly_fiji_west_19710108_20210501_klmin0_atot.nc".format(input_dir, species),
                       "{0}/output_{1}_ERA5_Land_monthly_new_caledonia_19710108_20211231_klmin0_atot.nc".format(input_dir, species)]
    for input_filepath in input_filepaths:
        output_filepath = input_filepath.replace(".nc", "_formatted.nc")
        if not exists(output_filepath):
            ds = xr.open_dataset(input_filepath, engine='netcdf4')
            ar = ds['atot'][:,:,:]
            ar = np.where(ar > 1e30, ar + np.nan, ar)
            ar = ar / np.nanmax(ar)
            ds['atot'][:,:,:] = ar / np.nanmax(ar)
            ds.to_netcdf(output_filepath)


# import matplotlib.pyplot as plt
# fig, ax = plt.subplots()
# ax.imshow(ar[1,:,:])
