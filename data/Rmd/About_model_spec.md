---
title: "About_model_spec"
output: html_document
---

### Regional model

The Regional model of introduction risk (cf **Early Warning System** and **Regional model** tabs) estimates the number of infected passengers arriving in each country of the Pacific region, using the methodology from [Liebig et al. 2019](https:www.doi.org/10.1371/journal.pone.0225193), and based on:
  - the yearly incidence in the surrounding countries ([WHO data](https://ntdhq.shinyapps.io/dengue5/)), corrected to account for under-reporting (using data from the [GBD project](https://vizhub.healthdata.org/gbd-results/))
  - the monthly numbers of passengers per flight connections between countries (monthly averages from data supplied by [OAG](https://www.oag.com/) on the 2017-2019 period)
  - the proportion of visitors and returning residents among each countries incoming flight passengers (estimated from data from [WTO](//www.unwto.org/statistic/basic-tourism-statistics)). This allows to estimate the time each passenger spend in the origin country, thus the probability of it being infected and still be infectious when it arrives at destination).
  - the countries-specific seasonality of the Dengue fever season (from [IAMAT](https://www.iamat.org/risks/dengue)), allowing to estimate countries monthly incidence from yearly incidence ([WHO data](https://ntdhq.shinyapps.io/dengue5/))

### Local models
Local models where built in New Caledonia and Fiji based on local data




#### New Caledonia
The local model consisted in separating outbreak years from non-outbreak years, using local climate and environmental data.</br>
The model quality of prediction was assessed through Leave-One-Out cross-validation.</br>
Outbreak years: 1976, 1977, 1980, 1986, 1989, 1990, 1995, 1996, 1998, 1999, 2003, 2004, 2008, 2009, 2012, 2013, 2014, 2016, 2017, 2018, 2019</br>
Predictor variables:
  - Climate data from the [NCEP](https://journals.ametsoc.org/view/journals/clim/27/6/jcli-d-12-00823.1.xml) model. Extracted at point location of Suva (178.44°E, 18.13°S).


#### Fiji
The local model consisted in separating outbreak years from non-outbreak years, using local climate and environmental data.</br>
The model quality of prediction was assessed through Leave-One-Out cross-validation.</br>
Outbreak years: 1972, 1975, 1980, 1984, 1986, 1990, 1998, 2003, 2008, 2014, 2017</br>
Predictor variables:
  - Climate data from the [NCEP](https://journals.ametsoc.org/view/journals/clim/27/6/jcli-d-12-00823.1.xml) climate model. Extracted at point location of Nouméa (166.46°E, 22.27°S).

### Mosquito vector density model
*Ae. aegypti* and *Ae. albopictus* densities were simulated based on temperature and rainfall data ([ERA5-Land](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview) climate model data), using a process-based model, described in Bonnin et al. 2022 (In Review)


### Correction of WHO data, accounting for Under-Reporting
Two sources were used to get countries yearly number of Dengue cases:
  - [WHO data](https://ntdhq.shinyapps.io/dengue5/) which consists in official reports
  - [GBD data](https://vizhub.healthdata.org/gbd-results/) which consists in [model](https://www.doi.org/10.1016/S1473-3099\(16\)00026-8) data designed to account for country-specific under-reporting rates.  

While [WHO data](https://ntdhq.shinyapps.io/dengue5/) reflects inter-annual variability, [GBD data](https://vizhub.healthdata.org/gbd-results/) better reflects the magnitude of Dengue cases. In order to get data that reflects both inter-annual variability and the magnitude of Dengue cases in countries, WHO yearly data was corrected using a country-specific under-reporting coefficient extracted from [GBD data](https://vizhub.healthdata.org/gbd-results/). [GBD data](https://vizhub.healthdata.org/gbd-results/) was downloaded over the 1990-2018 period and compared to WHO official report data to get country-specific under-reporting coefficients. Country-specific under-reporting coefficients were then applied to WHO yearly reported cases.


  
     
  
    
