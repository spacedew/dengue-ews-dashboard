---
title: "About_app"
output: html_document
---

### Regional model
The regional model of dengue introduction provides the origin and estimated number of infected passengers arriving in each country of the Pacific region, based on:
  - flight passenger flows
  - Dengue incidence in the connected countries.  
Details on the model are available in the **Model specifications** tab.

### Local models
Country-specific models of Dengue outbreak risk, based on climate and environmental variables, are described in this tab.  
Models were mostly trained to differentiate outbreak years from non-outbreak years, based on climate and environment variables.  
The best predictive model selected for each country are described in this tab:
  - Information about model quality (estimated through Leave-One-Out cross-validation):
    - Accuracy: Proportion of correct model predictions (positive and negative)
    - True Positive Rate (TPR): Ratio of correct positive model predictions over all positive observations
    - True Negative Rate (TNR): Ratio of correct negative model predictions over all negative observations
    - Balanced accuracy: Average of TPR and TNR
    - AUC: a measure of the ability of a model to distinguish between classes (0.5 < AUC < 1)
  - Partial dependency to predictor variables: how predictor variable values influence model predictions
  - Comparison of observations and model predictions. Monthly cases data are also supplied, even though model were fitted to yearly outbreak/no-outbreak data.

### Early Warning System
Three separate aspects of Dengue risk are presented in this tab:
  - **Risk of introduction** from surrounding countries  
The number of potential infected passengers arriving in the country is modeled here when informing dengue circulation in specific countries.
When selecting a country, a default value for incidence is supplied based on the median incidence observed in the country ([WHO data](https://ntdhq.shinyapps.io/dengue5/) corrected for under-reporting). Other values based on the user knowledge of the current situation can be supplied instead.

  - **Risk of local spread** based on local climate and environment variables  
Outputs of the local predictive model are supplied based on predictor variables values.
Default values are supplied based on available real-time data.

  - **Vulnerability of the population** based on immunity  
If available, the proportion of the population immune to a specific Dengue serotype is provided here, based on the most recent serosurveys.

### Seasonal dynamics
The seasonality of Dengue incidence in different countries is displayed here, along with climate and environment variables.

### Serotypes
Circulating Dengue serotypes in the countries of the Pacific Region, up to 2009, are displayed here.
For specific countries, historical series were supplemented with data gathered from various sources.

### Vectors
Here are displayed the distribution of the different vector species.

### Climate & env. data
Spatial and temporal variations of climate and vector densities, from [ERA5-Land](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview) climate model and weather stations, are displayed here.</br>
Time-series from climate model data can be accessed by clicking on any location of the map.</br>
Time-series extracted from weather stations can be accessed by clicking on any station location.</br>

### Dengue data
Here, the various sources of Dengue data available for each country and can be displayed in table and plot.</br>
Dengue data sources:
  - [WHO data](https://ntdhq.shinyapps.io/dengue5/) at yearly scale.
  - [GBD data](https://vizhub.healthdata.org/gbd-results/): modelled data from officially reported cases and adjusted for under-reporting. This data does not reflect accurately yearly dynamics of the disease.
  - Data gathered at monthly scale from countries official reports at sub-yearly scale.

### Real-time environmental data
Climate data is downloaded from the [NCEP](https://journals.ametsoc.org/view/journals/clim/27/6/jcli-d-12-00823.1.xml) climate model at point locations on a daily basis.

  
     
  
    
