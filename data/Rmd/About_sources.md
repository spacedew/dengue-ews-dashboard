---
title: "About_sources"
output: html_document
---

### Global Burden of Disease project (GBD)
Global Burden of Disease Collaborative Network.  
Global Burden of Disease Study 2019 (GBD 2019) Results.  
Seattle, United States: Institute for Health Metrics and Evaluation (IHME), 2020.  
Available from https://vizhub.healthdata.org/gbd-results/.  
Stanaway JD, Shepard DS, Undurraga EA, Halasa YA, Coffeng LE, Brady OJ, et al. The global burden of dengue: an analysis from the Global Burden of Disease Study 2013. The Lancet Infectious Diseases. 2016 Jun 1;16(6):712–23. DOI: [10.1016/S1473-3099(16)00026-8](https://www.doi.org/10.1016/S1473-3099\(16\)00026-8)

### Meteorological data
  - Fiji
    - Station historical data from [the Ministry of infrastructure, transport, disaster management and meteorological services](wwwo.moit.gov.fj)
    - Station real-time data from [WeatherOnline](https://www.weatheronline.co.uk)
  - New Caledonia: Station data from [Météo France](https://www.meteo.nc)

### Climate model data
  - [ERA5-Land](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview):  
Muñoz Sabater, J., (2021): ERA5-Land hourly data from 1950 to 1980. Copernicus Climate Change Service (C3S) Climate Data Store (CDS). (Accessed on 15-03-2022) DOI: [10.24381/cds.e2161bac](https://www.doi.org/10.24381/cds.e2161bac)
  - [NCEP](https://journals.ametsoc.org/view/journals/clim/27/6/jcli-d-12-00823.1.xml):  
Saha S, Moorthi S, Wu X, Wang J, Nadiga S, Tripp P, et al. The NCEP Climate Forecast System Version 2. Journal of Climate. 2014 Mar 15;27(6):2185–208. DOI: [10.1175/JCLI-D-12-00823.1](https://doi.org/10.1175/JCLI-D-12-00823.1)

### Regional model of dengue introduction
Liebig J, Jansen C, Paini D, Gardner L, Jurdak R. A global model for predicting the arrival of imported dengue infections. PLOS ONE. 2019 Dec 4;14(12):e0225193. DOI: [10.1371/journal.pone.0225193](https://www.doi.org/10.1371/journal.pone.0225193) 

### Serosurveys
Teurlai M. (2014) Modélisation multi-échelle de la dynamique spatiale de la Dengue : application à la Nouvelle-Calédonie et à la région Pacifique. Montpellier 2. Available at: http://www.theses.fr/2014MON20167

Kucharski AJ, Kama M, Watson CH, Aubry M, Funk S, Henderson AD, et al. Using paired serology and surveillance data to quantify dengue transmission and control during a large outbreak in Fiji. Ferguson NM, Jha P, editors. eLife. 2018 Aug 14;7:e34848. DOI: [10.7554/eLife.34848](https://www.doi.org/10.7554/eLife.34848)

### Vector species distribution
Guillaumot L. (2005) Arboviruses and their vectors in the Pacific - status report, Pacific Health Surveillance and Response, 12(2), pp. 45–52

### Mosquito vector density model
Bonnin et al. (2022). Predicting the effects of climate change on dengue vectors densities in Southeast Asia through process-based modeling. Environmental Health Perspectives. In Review.  
Tran A, Mangeas M, Demarchi M, Roux E, Degenne P, Haramboure M, et al. Complementarity of empirical and process-based approaches to modelling mosquito population dynamics with Aedes albopictus as an example—Application to the development of an operational mapping tool of vector populations. PLOS ONE. 2020 Jan 17;15(1):e0227407. DOI: [10.1371/journal.pone.0227407](https://www.doi.org/10.1371/journal.pone.0227407) 

  
     
  
    
