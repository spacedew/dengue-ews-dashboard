server <- function(session, input, output) {
  ########################
  #### Regional model ####
  ########################
  output$regional_model_tempserie <- renderPlotly({
    ggp <- regional_model_temp_serie(input$choice_country_regional, 
                                     input$choice_regional_dataset)
    plotly::ggplotly(ggp, 
                     tooltip="text") %>%
      plotly::layout(hovermode='closest', autosize=T)
  })
  
  output$regional_model_piechart <- renderPlotly({
    regional_model_pie_chart(input$choice_country_regional, 
                             input$choice_regional_dataset)
  })
  
  output$regional_model_map <- renderLeaflet({
    map_regional_model_data[[input$choice_regional_dataset]] %>%
      map_regional_model
  })
  
  output$regional_model_map_piecharts <- renderPlot({
    map_regional_model_piecharts[[input$choice_regional_dataset]]
  })
  
  ########################
  ##### Local models #####
  ########################
  output$local_model_info <- renderUI({
    iso_ = country_info$iso[match(input$choice_country_local, country_info$country)]
    variables <- data_local_model %>%
      subset(iso==iso_) %>%
      pull(model_objects) %>%
      pull(variables) %>%
      magrittr::extract2(1)
    qm <- data_local_model %>%
      subset(iso==iso_) %>%
      pull(quality_metrics)  
    local_model_info <- sprintf("
                                <center>
                                  <big><b>Model formula</b></br>outbreak ~ %s</big>
                                  </br></br>
                                  <big><b>Predictor variables:</b></big></br>
                                  %s
                                  </br></br>
                                  <big><b>Model quality</b></big></br>
                                  Leave-one-out cross-validation</br><b>Accuracy</b> = %s
                                  </br>
                                  <b>Balanced accuracy</b> = %s
                                  </br>
                                  <b>True positive rate</b> = %s
                                  </br>
                                  <b>True negative rate</b> = %s
                                </center>",
                                paste(variables, collapse = " + "),
                                paste(sapply(variables, function(v) gsub("\n", " ", model_variable_name(v))), collapse = "</br>"),
                                qm$accuracy, 
                                qm$balanced_accuracy, 
                                qm$sensitivity,
                                qm$specificity)
    HTML(local_model_info)
  })
  
  output$local_model_plotAUC <- renderPlotly({
    iso_ = country_info$iso[match(input$choice_country_local, country_info$country)]
    data_local_model %>%
      subset(iso == iso_) %>%
      pull(auc_plotly) %>%
      magrittr::extract2(1)
  })
  
  output$local_model_partial_dep <- renderPlotly({
    iso_ = country_info$iso[match(input$choice_country_local, country_info$country)]
    data_local_model %>%
      subset(iso == iso_) %>%
      pull(partial_dep_plotly) %>%
      magrittr::extract2(1)
  })
  
  output$local_model_plot <- renderPlotly({
    iso_ = country_info$iso[match(input$choice_country_local, country_info$country)]
    data_local_model %>%
      subset(iso == iso_) %>%
      pull(predictions_plotly) %>%
      magrittr::extract2(1)
  })
  
  output$local_model_plot2 <- renderPlotly({
    iso_ = country_info$iso[match(input$choice_country_local, country_info$country)]
    data_local_model %>%
      subset(iso == iso_) %>%
      pull(predictions_plotly2) %>%
      magrittr::extract2(1)
  })
  
  ########################
  # Early Warning System #
  ########################
  index_ews_regional <- reactiveValues(i=3)
  ews_regional_country_names <- reactive(paste0("regional_model_country", seq_len(index_ews_regional$i)))
  ews_regional_country_incidences <- reactive(paste0("regional_model_incidence", seq_len(index_ews_regional$i)))
  output$ui_ews_regional <- renderUI({
    ui_ews_regional(index_ews_regional, input, initial_settings=T)
  })
  
  observeEvent(input$add_ews_country, {
    index_ews_regional$i <- index_ews_regional$i + 1
    output$ui_ews_regional <- renderUI({
      ui_ews_regional(index_ews_regional, input, initial_settings=F, add_country=T)
    })
  })
  # observeEvent(input[[sprintf("regional_model_country%s", index_ews_regional$i)]],{
  #   iso <- country_info$iso[match(input[[sprintf("regional_model_country%s", index_ews_regional$i)]], country_info$country)]
  #   val <- median_incidence_who$median_incidence[median_incidence_who$iso==iso]
  #   updateNumericInput(session, sprintf("regional_model_incidence%s", index_ews_regional$i), value = val)
  # })
  observeEvent(input$reset_ews_regional, {
    index_ews_regional$i <- 3
    output$ui_ews_regional <- renderUI({
      ui_ews_regional(index_ews_regional, input, initial_settings=T)
    })
  })
  observeEvent(input$delete_ews_regional, {
    index_ews_regional$i <- 0
    output$ui_ews_regional <- renderUI({
      ui_ews_regional(index_ews_regional, input, initial_settings=F, empty=T)
    })
  })
  observeEvent(input$choice_country_ews, {
    index_ews_regional$i <- 3
    output$ui_ews_regional <- renderUI({
      ui_ews_regional(index_ews_regional, input, initial_settings=T)
    })
  })
  
  
  output$ui_ews_local <- renderUI({     
    country <- input$choice_country_ews
    iso_ <- country_info$iso[match(country, country_info$country)]
    variables <- data_local_model %>%
      subset(iso==iso_) %>%
      pull(model_objects) %>%
      pull(variables) %>%
      magrittr::extract2(1)
    variable_range <- data_local_model %>%
      subset(iso==iso_) %>%
      pull(variable_range) %>%
      magrittr::extract2(1) %>%
      mutate(mean=(min+max)/2)
    initial_values <- lapply(variables, function(x) {
      if (x == 'years_since_last_epidemic'){
        return(years_since_last_epidemic[[iso_]])
      }else{
        if (any(grepl("_ncep", x))){source_="NCEP"}
        if (any(grepl("_mf", x))){source_="MeteoFrance"}
        rt_data <- realtime_data %>%
          subset(iso==iso_ & source==source_)
        if (nrow(rt_data) != 0){
          return(rt_data %>%
                    pull(summup) %>%
                    magrittr::extract2(1) %>%
                    tail(1) %>%
                    pull(x))
        }else{
            return(variable_range$mean[variable_range$variable==x])
          }
      }
    }) %>%
      purrr::set_names(variables) %>%
      as.data.frame() %>%
      mutate(across(everything(), ~round(.x, digits=1)))
    
    
    uis = tagList()
    for (i in seq_along(variables)){
      v <- variables[i]
      step_ = ifelse(v=='years_since_last_epidemic', 1, 
                     ifelse(grepl('rh', v), 0.01, 0.1))
      uis[[i]] = sliderInput(inputId = sprintf("local_model_value%s", which(v==variables)), 
                             label = model_variable_name(v), 
                             value = initial_values[[v]],
                             min = variable_range$min[variable_range$variable==v],
                             max = variable_range$max[variable_range$variable==v],
                             step=step_,
                             round = 1)
    }
    uis
  })
  
  output$ews_output_regional <- renderUI({
    if (index_ews_regional$i==0){
      html <- "
    <p>
      <center>
        Please add origin countries and their corresponding incidence 
      </center>
    </p>"
    }else{
      countries <- map_chr(ews_regional_country_names(), ~ input[[.x]] %||% "")
      incidences <- map_chr(ews_regional_country_incidences(), ~ input[[.x]] %||% "")
      input_df <- data.frame(country=countries, incidence=incidences) %>%
        mutate(incidence = as.numeric(incidence)) %>%
        left_join(country_info %>% dplyr::select(country, iso), by="country")
      iso_destination <- country_info$iso[match(input$choice_country_ews, country_info$country)]
      m <- Sys.Date() %>%
        format("%m") %>%
        as.numeric()
      html <- compute_regional_model(input_df, m, iso_destination) 
    }
    HTML(html)
  })
  
  output$ews_output_local <- renderUI({
    country <- input$choice_country_ews
    iso_ <- country_info$iso[match(country, country_info$country)]
    model_objects <- data_local_model %>%
      subset(iso==iso_) %>%
      pull(model_objects)
    variables <- model_objects %>%
      pull(variables) %>%
      magrittr::extract2(1)
    preproc <- model_objects %>%
      pull(preproc) %>%
      magrittr::extract2(1)
    list_svm_probT <- model_objects %>%
      pull(list_svm_probT) %>%
      magrittr::extract2(1)
    list_svm <- model_objects %>%
      pull(list_svm) %>%
      magrittr::extract2(1)
    qm <- data_local_model %>%
      subset(iso==iso_) %>%
      pull(quality_metrics)
    
    values <- lapply(1:length(variables), function(i) input[[sprintf("local_model_value%s",i)]])
    data <- data.frame(variables = variables) %>%
      mutate(value = as.numeric(values)) %>%
      pivot_wider(names_from=variables)
    preproc_data <- preproc %>%
      bake(new_data=data)
    
    # Class predictions
    preds <- foreach(i = 1:length(list_svm), .combine='c') %do% {
      svm = list_svm[[i]]
      predict(svm, newdata = preproc_data)
    } %>%
      as.numeric()
    pred_median <- median(preds)
    prop_pred_median <- 100*length(which(preds==pred_median)) / length(preds)
    pred_median <- c('No outbreak', "Outbreak")[pred_median]
    
    # Probability predictions
    preds_p <- foreach(i = 1:length(list_svm), .combine='c') %do% {
      svm = list_svm_probT[[i]]
      predict(svm, newdata = preproc_data, type='probabilities')[,2]
    }
    alpha=0.05; z = qnorm(1-alpha/2)
    CI = mean(preds_p) + c(-1,1)*z*sqrt(mean(preds_p)*(1-mean(preds_p))/length(preds_p)) 
    CI <- round(CI*100, digits=1)
    pred_p_mean <- round(mean(preds_p) * 100, digits=1)
    
    
    html <- sprintf("
                    <center>
                      <p>
                        <big><big>Models based on local environment variables</big></big>
                      </p>
                    </center>
                      This year (%s):</br>
                    <center>
                      <p>
                        <big>Model prediction: <b>%s</b></big>
                        </br>
                        Predicted by <b>%s</b>%% of the CV models
                        </br>
                        Average accuracy of the models: <b>%s</b>%% (TPR = <b>%s</b>%%, TNR = <b>%s</b>%%)
                      </p>
                      <!--<p>
                        <big>Model probabilities: <b><b>%s</b>%%</b></big>
                        </br>
                        95%% CI: <b>%s</b> - <b>%s</b>%%
                      </p>-->
                    </center>
                    ",
                    strftime(Sys.time(), format="%Y"),
                    pred_median, prop_pred_median,
                    100*qm$accuracy, 100*qm$sensitivity, 100*qm$specificity,
                    pred_p_mean, CI[1], CI[2])
    HTML(html)
  })
  
  output$ews_output_immunity <- renderUI({
    country <- input$choice_country_ews
    iso_ <- country_info$iso[match(country, country_info$country)]
    sero <- input$choice_serotype_ews
    seroprev <- tibble_serotype_data %>%
      subset(iso==iso_) %>%
      pull(seroprev)
    if (length(seroprev)!=0){
      seroprev <- seroprev  %>%
        magrittr::extract2(1)
      if (country == "Fiji"){
        year = 2015
        seroprev <- seroprev %>%
          subset(Serotype==sero) %>%
          dplyr::select(`Seroprevalence 2015`)
      }
      
      html <- sprintf("
                    <p>
                      <center>
                        Last seroprevalence survey: %s
                        </br>
                        Population immunity to <big>%s</big>: 
                        </br>
                        <big>%s</big>
                      </center>
                    </p>",
                      year, sero, seroprev)
    }else{
      html <- sprintf("
                    <p>
                      <center>
                        No seroprevalence data available in %s.
                      </center>
                    </p>", country)
    }
    
    
    HTML(html)
  })

  ########################
  ### Seasonal dynamics ##
  ########################
  output$seasonal_trend <- renderPlotly({
    iso_ = country_info$iso[match(input$choice_country_seasonaltrend, country_info$country)]
    variables = names(variables_names_seasonal[variables_names_seasonal %in% input$choice_variables_seasonaltrend])
    seasonal_trend(iso_, variables)
  })
  
  
  
  ########################
  ####### Serotypes ######
  ########################
  output$pl_serotype <- renderPlotly({
    pl_sero
  })
  
  output$historic_serotype_country <- renderPlotly({
    country <- input$choice_sero_country
    historic_sero_country(country)
  })
  
  output$seroprevalence_country <- renderDataTable({
    country <- input$choice_sero_country
    table_seroprev(country)
  }, rownames=FALSE)
  
  ########################
  ####### Vectors ########
  ########################
  output$map_vectors <- renderLeaflet({
    var <- input$choice_vector
    map_species(var)
  })

  ########################
  # Climate & env. data ##
  ########################
  rv <- reactiveValues(points=list(lat=default_map_extractpoints$lat[default_map_startpoints$iso=="FJI"], 
                                   lng=default_map_extractpoints$lng[default_map_startpoints$iso=="FJI"]),
                       station=default_stations$station[default_stations$iso=="FJI"],
                       whichraster="FJI",
                       centermap=as.numeric(default_map_startpoints[default_map_startpoints$iso=="FJI", c(2:4)]))
  
  observeEvent(input$map_climate_click, {
    click <- input$map_climate_click
    rv$points <- click
    newraster <- which_raster(rv$points)
    updateSelectInput(session, "model_station", selected = "Model data")
    if (rv$whichraster != newraster){
      rv$whichraster <- newraster
      rv$centermap <- as.numeric(default_map_startpoints[default_map_startpoints$iso==newraster, c(2:4)])
      rv$station <- default_stations$station[default_stations$iso==newraster]
      updateRadioButtons(session, "dengue_country", selected = country_info$country[match(newraster, country_info$iso)])
    }
  })
  observeEvent(input$map_climate_marker_click, {
    click <- input$map_climate_marker_click
    rv$station <- station_data$station[which(station_data$lat == click$lat & station_data$lon == click$lng)]
    updateSelectInput(session, "model_station", selected = "Station data")
    rv$points <- click
    newraster <- which_raster(click)
    if (rv$whichraster != newraster){
      rv$whichraster <- newraster
      rv$centermap <- as.numeric(default_map_startpoints[default_map_startpoints$iso==newraster, c(2:4)])
      updateRadioButtons(session, "dengue_country", selected = country_info$country[match(newraster, country_info$iso)])
    }
  })
  observeEvent(input$map_vector_click, {
    click <- input$map_vector_click
    rv$points <- click
    newraster <- which_raster(rv$points)
    updateSelectInput(session, "model_station", selected = "Model data")
    if (rv$whichraster != newraster){
      rv$whichraster <- newraster
      rv$centermap <- as.numeric(default_map_startpoints[default_map_startpoints$iso==newraster, c(2:4)])
      rv$station <- default_stations$station[default_stations$iso==newraster]
      updateRadioButtons(session, "dengue_country", selected = country_info$country[match(newraster, country_info$iso)])
    }
  })
  observeEvent(input$dengue_country, {
    newraster <- country_info$iso[match(input$dengue_country, country_info$country)]
    if (rv$whichraster != newraster){
      rv$whichraster <- newraster
      rv$centermap <- as.numeric(default_map_startpoints[default_map_startpoints$iso==newraster, c(2:4)])
      rv$points <- list(lat=default_map_extractpoints$lat[default_map_startpoints$iso==newraster], 
                        lng=default_map_extractpoints$lng[default_map_startpoints$iso==newraster])
      rv$station <- default_stations$station[default_stations$iso==newraster]
    }
  })
  observeEvent(input$dimension, {
    window_dim <- input$dimension
    return(window_dim)
  })
  observeEvent(input$model_station, {
    if (input$model_station == 'Station data'){
      rv$points <- list(lat=station_data$lat[station_data$station==rv$station],
                        lng=station_data$lon[station_data$station==rv$station])
    }
  })
  
  output$historical_data_point <- renderUI({
    if (input$model_station=="Model data"){
      html <- sprintf('Model data extracted on <big>%s°E, %s°N</big> -> Click on maps to change extract point', 
                      round(rv$points$lng, digits=2), round(rv$points$lat, digits=2))
    }
    if (input$model_station=="Station data"){
      html <- sprintf('Station data: <big>%s</big> -> Click on map to change station', 
                      rv$station)
    }
    if (input$model_station=="Both"){
      html <- sprintf('Station data: <big>%s</big>, closest raster pixel: <big>%s°E, %s°N</big> -> Click on map to change station', 
                      rv$station, round(rv$points$lng, digits=2), round(rv$points$lat, digits=2))
    }
    HTML(html)
  })
  
  output$map_climate <- renderLeaflet({
    if (input$map_climate_variable == "Temperature"){
      layer = do.call(merge, unname(layer_t2m))
      title = "Temperature average (1971-2018)"
      legend_title = "Temperature (°C)"
    }else if (input$map_climate_variable == "Rainfall"){
      layer = do.call(merge, unname(layer_tp))
      title = "Rainfall average (1971-2018)"
      legend_title = "Rainfall (mm/month)"
    }
    labs_stations <- as.list(station_data$station)
    leaflet() %>%
      addProviderTiles("CartoDB.Positron") %>%
      # setView(178.65, -17.36, zoom = 8) %>%
      setView(rv$centermap[1], rv$centermap[2], zoom = rv$centermap[3]) %>%
      addCircleMarkers(
        lng = rv$points$lng, lat=rv$points$lat,
        radius = 8,
        label = HTML("Extract model data here"),
        fillColor = "white",
        color = "#444444",
        weight=2,
        stroke = TRUE, fillOpacity = 0.8) %>%
      addControl(tags$div(title, class='leaflet-small-title'), position = "topright", className="map-title") %>%
      addRasterImage(x = layer , 
                     colors = colorNumeric(pal, c(layer[]),  na.color = "transparent"), 
                     opacity = 0.9) %>%
      addCircleMarkers(data=station_data,
                       lng=~lon, lat=~lat,
                       layerId=~station,
                       radius=10,
                       label = lapply(labs_stations, function(x) HTML(sprintf("Station data: %s", x))),
                       fillColor = "#161fd6",
                       color = "#444444",
                       weight=2, 
                       stroke = TRUE, fillOpacity = 0.8) %>% 
      addLegend(pal = colorNumeric(pal, c(layer[]),  na.color = "transparent"),
                values=range(layer[], na.rm=T), title=legend_title)
  })
  output$map_vector <- renderLeaflet({
    if (input$map_vector_variable == "Ae. albopictus"){
      layer = do.call(merge, unname(layer_albo))
      title = "Ae. albopictus density average (1971-2018)"
    }else if (input$map_vector_variable == "Ae. aegypti"){
      layer = do.call(merge, unname(layer_aeg))
      title = "Ae. aegypti density average (1971-2018)"
    }
    legend_title = "Density index"
    leaflet() %>%
      addProviderTiles("CartoDB.Positron") %>%
      # setView(178.65, -17.36, zoom = 8) %>%
      setView(rv$centermap[1], rv$centermap[2], zoom = rv$centermap[3]) %>%
      addCircleMarkers(
        lng = rv$points$lng, lat=rv$points$lat,
        radius = 8,
        label = HTML("Extract model data here"),
        fillColor = "white",
        color = "#444444",
        weight=2,
        stroke = TRUE, fillOpacity = 0.8) %>%
      addControl(tags$div(title, class='leaflet-small-title'), position = "topright", className="map-title") %>%
      addRasterImage(x = layer ,
                     colors = colorNumeric(pal, c(layer[]),  na.color = "transparent"),
                     opacity = 0.9) %>% 
      addLegend(pal = colorNumeric(pal, c(layer[]),  na.color = "transparent"),
                values=range(layer[], na.rm=T), title=legend_title)
  })
  
  output$plot <- renderHighchart({
    point <- SpatialPoints(cbind(rv$points$lng, rv$points$lat), proj4string=CRS("+proj=longlat +datum=WGS84"))
    if (input$model_station %in% c("Model data","Both")){
      point_value_rainfall <- raster::extract(ERA5_tp[[rv$whichraster]], point)[1,] %>%
        raster::as.data.frame() %>% 
        tibble::rownames_to_column("date") %>% 
        mutate(date = as.Date(date, format = "X%Y.%m.%d")) %>%
        purrr::set_names(c("date","rainfall"))
      point_value_temperature <- raster::extract(ERA5_t2m[[rv$whichraster]], point)[1,] %>%
        raster::as.data.frame() %>% 
        tibble::rownames_to_column("date") %>% 
        mutate(date = as.Date(date, format = "X%Y.%m.%d")) %>%
        purrr::set_names(c("date","temperature")) %>%
        mutate(temperature = temperature - 273.15)
      point_value_aeg <- raster::extract(ERA5_aeg[[rv$whichraster]], point)[1,] %>%
        raster::as.data.frame() %>% 
        tibble::rownames_to_column("date") %>% 
        mutate(date = as.Date(date, format = "X%Y.%m.%d")) %>%
        purrr::set_names(c("date","aegypti"))
      point_value_albo <- raster::extract(ERA5_albo[[rv$whichraster]], point)[1,] %>%
        raster::as.data.frame() %>% 
        tibble::rownames_to_column("date") %>% 
        mutate(date = as.Date(date, format = "X%Y.%m.%d")) %>%
        purrr::set_names(c("date","albopictus"))
    }
    if (input$model_station %in% c("Station data","Both")){
      st_data <- station_data %>%
        subset(station==rv$station) %>%
        pull(data) %>%
        magrittr::extract2(1)
      station_value_temperature <- st_data %>%
        dplyr::select(date, temperature=temp)
      station_value_rainfall <- st_data %>%
        dplyr::select(date, rainfall=rain)
    }
    
    country_dengue_who_data <- tibble_dengue_data %>%
      subset(iso == country_info$iso[match(input$dengue_country, country_info$country)]) %>%
      pull(WHO) %>%
      magrittr::extract2(1)
    
    dengue_monthly <- tibble_dengue_data %>%
      subset(iso == country_info$iso[match(input$dengue_country, country_info$country)]) %>%
      pull(`Country data`) %>%
      magrittr::extract2(1)
    if (is.null(dengue_monthly)){
      dengue_monthly = data.frame(date=NA, cases=NA)
    }
    
    hc <- highchart(type = "stock") %>%
      hc_title(text = "")
    if (input$model_station %in% c("Model data", "Both")){
      hc <- hc %>%
        hc_add_series(point_value_rainfall,
                      type = "column",
                      hcaes(x = date, y = rainfall),
                      yAxis = 0,
                      name = "Rainfall (model data)",
                      color = "#03aaf9") %>%
        hc_add_yAxis(nid = 1L,
                     title = list(text = "Rainfall (mm)"),
                     relative = 1,
                     min=0,
                     showInLegend=F) %>%
        hc_add_series(point_value_temperature,
                      type = "line",
                      hcaes(x = date, y = temperature),
                      yAxis = 1,
                      name = "Temperature (model data)",
                      color = "#03aaf9") %>%
        hc_add_yAxis(nid = 2L,
                     title = list(text = "Temperature"),
                     relative = 1,
                     showInLegend=F)
      ya <- lapply(hc$x$hc_opts$series, function(x) x$yAxis) %>%
        unlist() %>%
        max()
      if (input$model_station == "Model data"){
        hc <- hc %>%
          hc_add_series(point_value_albo,
                        type = "line",
                        hcaes(x = date, y = albopictus),
                        yAxis = ya+1,
                        name = "Ae. albopictus density",
                        color = "#fab666",) %>%
          hc_add_yAxis(nid = 1L,
                       title = list(text =  "Ae. albopictus density"),
                       relative = int(ya),
                       showInLegend=F) %>%
          hc_add_series(point_value_aeg,
                        type = "line",
                        hcaes(x = date, y = aegypti),
                        yAxis = ya+2,
                        name = "Ae. aegypti density",
                        color = "#fab666") %>%
          hc_add_yAxis(nid = 2L,
                       title = list(text = "Ae. aegypti density"),
                       relative = int(ya+1),
                       min=0,
                       showInLegend=F)
        
      }else{
        hc <- hc %>%
          hc_add_series(station_value_rainfall,
                        type = "column",
                        hcaes(x = date, y = rainfall),
                        yAxis = 0,
                        name = "Rainfall (Station data)",
                        color = "#0279b1") %>%
          hc_add_series(station_value_temperature,
                        type = "line",
                        hcaes(x = date, y = temperature),
                        yAxis = 1,
                        name = "Temperature (Station data)",
                        color = "#0279b1",)      
      }    
    }else{
      hc <- hc %>%
        hc_add_series(station_value_rainfall,
                      type = "column",
                      hcaes(x = date, y = rainfall),
                      yAxis = 0,
                      name = "Rainfall (Station data)",
                      color = "#0279b1") %>%
        hc_add_yAxis(nid = 1L,
                     title = list(text = "Rainfall (mm)"),
                     relative = 1,
                     min=0,
                     showInLegend=F) %>%
        hc_add_series(station_value_temperature,
                      type = "line",
                      hcaes(x = date, y = temperature),
                      yAxis = 1,
                      name = "Temperature (Station data)",
                      color = "#0279b1") %>%
        hc_add_yAxis(nid = 2L,
                     title = list(text = "Temperature"),
                     relative = 1,
                     showInLegend=F)
      
    }
    ya <- lapply(hc$x$hc_opts$series, function(x) x$yAxis) %>%
      unlist() %>%
      max()
    hc <- hc %>%
      hc_add_series(dengue_monthly,
                    type = "line",
                    hcaes(x = date, y = cases),
                    yAxis = ya+1,
                    name = "Dengue monthly cases",
                    color = "#ba0e0e") %>%
      hc_add_yAxis(nid = int(ya),
                   title = list(text = "Dengue monthly cases"),
                   relative = 1) %>%
      hc_add_series(country_dengue_who_data,
                    type = "line",
                    hcaes(x = date, y = cases),
                    yAxis = ya+2,
                    name = "Dengue yearly cases (WHO)",
                    color = "#ba0e0e") %>%
      hc_add_yAxis(nid = int(ya+1),
                   title = list(text = "Dengue yearly cases (WHO)"),
                   relative = 1) %>%
      hc_xAxis(type = "datetime") %>%
      hc_rangeSelector(buttons = list(
        list(type = 'all', text = 'All'),
        list(type = 'year', count = 10, text = '10 y.'),
        list(type = 'year', count = 5, text = '5 y.'),
        list(type = 'year', count = 1, text = '1 y.')
      ),
      selected = 0,
      verticalAlign = "bottom") %>%
      hc_legend(enabled = TRUE) %>%
      hc_tooltip(useHTML=T,
        valueDecimals=1)
    
    hc
  })
  
  
  ########################
  ##### Dengue data ######
  ########################
  output$update_dengue_data_panels <- renderUI({     # rendering all the panels called for by user
    country <- input$dengue_data_country
    iso <- country_info$iso[match(country, country_info$country)]
    choices <- available_data(iso)
    update <- radioButtons(inputId = "dengue_data_source", 
                           label = "Data source", 
                           choices = choices,
                           selected = choices[1])
    update
  })
  
  # output$dengue_data_plot <- renderPlotly({
  #   country <- input$dengue_data_country
  #   iso_ <- country_info$iso[match(country, country_info$country)]
  #   data_source <- input$dengue_data_source
  #   ggp <- tibble_dengue_data %>%
  #     subset(iso==iso_) %>%
  #     pull(all_of(data_source)) %>%
  #     magrittr::extract2(1) %>%
  #     as.data.frame() %>%
  #     ggplot(aes(date, cases, group=1,
  #                text = sprintf("%s\n%s cases\nIncidence = %s",
  #                               date, cases, round(incidence, digits=2)))) + 
  #     geom_line() + theme_bw() + labs(x="", y="Number of cases")
  #   plotly::ggplotly(ggp, 
  #                    tooltip="text") %>%
  #     plotly::layout(hovermode='closest', autosize=T)
  # })
  
  output$dengue_data_plot <- renderHighchart({
    country <- input$dengue_data_country
    iso_ <- country_info$iso[match(country, country_info$country)]
    data_sources <- tibble_dengue_data %>%
      subset(iso==iso_) %>%
      dplyr::select_if(~ !(is.list(.) && all(map_lgl(., is.null)))) %>%
      dplyr::select(-iso) %>%
      colnames
    
    hc <- highchart(type = "stock") %>%
      hc_title(text = "")
    ya = 0
    for (source in data_sources){
      data <- tibble_dengue_data %>%
        subset(iso==iso_) %>%
        pull(all_of(source)) %>%
        magrittr::extract2(1)      
      res = data$temp_resolution[1]
      if (res == 'yearly'){
        hc <- hc %>%
          hc_add_series(data,
                        type = "line",
                        hcaes(x = date, y = cases),
                        yAxis = ya,
                        name = sprintf("Yearly cases (%s)", source),
                        color = "#03aaf9") %>%
          hc_add_yAxis(nid = int(ya)+1,
                       title = list(text = sprintf("Yearly cases (%s)", source)),
                       relative = 1,
                       min=0,
                       showInLegend=F)
      } else if (res == 'monthly'){
        hc <- hc %>%
          hc_add_series(data,
                        type = "line",
                        hcaes(x = date, y = cases),
                        yAxis = ya,
                        name = sprintf("%s cases (%s)", str_to_title(res), source),
                        color = "#fab666") %>%
          hc_add_yAxis(nid = int(ya)+1,
                       title = list(text = sprintf("%s cases (%s)", str_to_title(res), source)),
                       relative = 1,
                       showInLegend=F)
      } 
      ya <- ya+1
    }
    hc %>%
      hc_xAxis(type = "datetime")  %>% 
      hc_rangeSelector(buttons = list(
        list(type = 'all', text = 'All'),
        list(type = 'year', count = 10, text = '10 y.'),
        list(type = 'year', count = 5, text = '5 y.'),
        list(type = 'year', count = 1, text = '1 y.')
      ),
      selected = 0,
      verticalAlign = "bottom") %>%
      hc_legend(enabled = TRUE) %>%
      hc_tooltip(useHTML=T,
                 valueDecimals=1)
  })
  
  
  output$dengue_data_table <- renderDataTable({
    country <- input$dengue_data_country
    iso_ <- country_info$iso[match(country, country_info$country)]
    data_source <- input$dengue_data_source
    tibble_dengue_data %>%
      subset(iso==iso_) %>%
      pull(all_of(data_source)) %>%
      magrittr::extract2(1) %>%
      as.data.frame() %>%
      rename_with(.fn=rename_tibble_columns)
  }, rownames=FALSE
  # ,
  # options = list(pageLength = 10, autoWidth = TRUE)
  )

  ########################
  # Real-time env. data ##
  ########################
  output$update_realtime_panels <- renderUI({     # rendering all the panels called for by user
    iso_ = country_info$iso[match(input$realtime_country, country_info$country)]
    choices = realtime_data %>%
      subset(iso==iso_) %>%
      pull(source) %>%
      unique()
    radioButtons(inputId = "realtime_data_source",
                 label = "Source",
                 choices = choices,
                 selected = choices[1],
                 inline=T)
  })
  
  output$source_realtime_data <- renderUI({
    iso_ = country_info$iso[match(input$realtime_country, country_info$country)]
    realtime_data %>%
      subset(iso==iso_ & source==input$realtime_data_source) %>%
      pull(html) %>%
      HTML()
  }) 
  
  output$realtime_data_plot <- renderHighchart({
    iso_ = country_info$iso[match(input$realtime_country, country_info$country)]
    data <- realtime_data %>%
      subset(iso==iso_ & source == input$realtime_data_source) %>%
      pull(raw) %>%
      magrittr::extract2(1)
    
    hc <- highchart(type = "stock") %>%
      hc_title(text = "")
    ya = 0
    for (v in colnames(data)[colnames(data) != 'date']){
      if (v == 'rain'){
        hc <- hc %>%
          hc_add_series(data,
                        type = "column",
                        hcaes(x = date, y = rain),
                        yAxis = ya,
                        name = "Rainfall",
                        color = "#03aaf9") %>%
          hc_add_yAxis(nid = int(ya)+1,
                       title = list(text = "Rainfall (mm)"),
                       relative = 1,
                       min=0,
                       showInLegend=F)
      } else if (v == 'temp'){
        hc <- hc %>%
          hc_add_series(data,
                        type = "line",
                        hcaes(x = date, y = temp),
                        yAxis = ya,
                        name = "Temperature",
                        color = "#fab666") %>%
          hc_add_yAxis(nid = int(ya)+1,
                       title = list(text = "Temperature (°C)"),
                       relative = 1,
                       showInLegend=F)
      } else if (v == 'rh'){
        hc <- hc %>%
          hc_add_series(data,
                        type = "line",
                        hcaes(x = date, y = rh),
                        yAxis = ya,
                        name = "Relative Humidity",
                        color = "#03aaf9") %>%
          hc_add_yAxis(nid = int(ya)+1,
                       title = list(text = "Relative Humidity (%)"),
                       relative = 1,
                       showInLegend=F)
      }
      ya <- ya+1
    }
    hc %>%
      hc_xAxis(type = "datetime")  %>% 
      hc_rangeSelector(buttons = list(
        list(type = 'all', text = 'All'),
        list(type = 'year', count = 10, text = '10 y.'),
        list(type = 'year', count = 5, text = '5 y.'),
        list(type = 'year', count = 1, text = '1 y.')
      ),
      selected = 3,
      verticalAlign = "bottom") %>%
      hc_legend(enabled = TRUE) %>%
      hc_tooltip(useHTML=T,
                 valueDecimals=1)
    
  })
  
    
  
  ########################
  ######### About ########
  ########################

}





